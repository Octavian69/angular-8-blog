import { IEnvironment } from './IEnvironment';

export const environment: IEnvironment = {
  production: false,
  apiKey: 'AIzaSyDX_PDZHPgAz7eacny4nuf30MVuQntQw2A',
  fbDbUrl: 'https://angular-8-blog.firebaseio.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
