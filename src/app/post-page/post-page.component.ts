import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from '../shared/services/posts.services';
import { Subscription } from 'rxjs';
import { IPost } from '../shared/interfaces/IPost';

@Component({
  selector: 'blog-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.scss']
})
export class PostPageComponent implements OnInit, OnDestroy {

  postId: string;
  post: IPost;

  subs$: Subscription[] = [];

  constructor(
    private activeRouter: ActivatedRoute,
    private postsService: PostsService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy(): void {
    this.subs$.forEach(s => s.unsubscribe());
    this.subs$ = [];
  }

  init(): void {
    this.postId = this.activeRouter.snapshot.paramMap.get('id');
    this.getPostById(this.postId);
  }

  getPostById(postId: string): void {
    const sub$: Subscription = this.postsService.getPostById(postId).subscribe((post: IPost) => {
      console.log(post)
      this.post = post;
    });

    this.subs$.push(sub$);
  }

}
