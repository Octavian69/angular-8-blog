import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { SimpleObject } from 'src/app/shared/types/Shared';
import { IUser } from 'src/app/shared/interfaces/iUser';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'blog-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  error$: Subject<string> = this.authService.error$;
  form: FormGroup;
  
  deniedMessage: string;
  submitted: boolean = false;
  subs$: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activeRouter: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.subs$.forEach(s => s.unsubscribe());
    this.subs$ = [];
    this.deniedMessage = null;
  }

  init(): void {
    this.initForm();
    this.initRouteSub();
  }

  initForm(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email], []],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  initRouteSub(): void {
    const sub$: Subscription = this.activeRouter.queryParamMap.subscribe((params: ParamMap) => {
      if(params.get('accessDenied')) {
        this.deniedMessage = 'Введи свои данные для входа в кабинет.';
      }

      if(params.get('authFailed')) {
        this.deniedMessage = 'Сессия истекла.Введите данные заново';
      }
    });

    this.subs$.push(sub$);
  }

  getErrorClass(controlName: 'email' | 'password'): SimpleObject {
    return {
      'invalid': this.isInvalid(controlName)
    }
  }

  isInvalid(controlName: 'email' | 'password'): boolean {
    const control: AbstractControl = this.form.get(controlName);

    return control.invalid && control.touched;
  }

  onSubmit(): void {
    const { value: { email, password }, valid } = this.form;

    this.submitted = true;

    if(valid) {
      const user: IUser = { email, password, returnSecureToken: true };
      
      const sub$: Subscription = this.authService.login(user).subscribe(_ => {
        this.router.navigateByUrl('/admin/dashboard');
        this.form.reset();
        this.submitted = false;
      },
        err => this.submitted = false
      )
    
      this.subs$.push(sub$);
    }
  }

}
