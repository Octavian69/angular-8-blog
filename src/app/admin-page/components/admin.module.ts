import { NgModule } from '@angular/core';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { CreatePageComponent } from './create-page/create-page.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { AdminRoutingModule } from './admin.routing.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';
import { FilterPipe } from 'src/app/shared/pipes/filter.pipe';
import { AlertComponent } from '../../shared/components/alert/alert.component';
import { AlertService } from 'src/app/shared/services/alert.service';

@NgModule({
    declarations: [
        AdminLayoutComponent,
        LoginPageComponent,
        DashboardPageComponent,
        CreatePageComponent,
        EditPageComponent,
        FilterPipe,
        AlertComponent
    ],
    imports: [
        SharedModule,
        AdminRoutingModule
    ],
    providers: [
        AuthGuard,
        AlertService
    ]
})
export class AdminModule{}