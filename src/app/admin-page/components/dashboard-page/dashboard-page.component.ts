import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostsService } from 'src/app/shared/services/posts.services';
import { Subscription, Observable } from 'rxjs';
import { IPost } from 'src/app/shared/interfaces/IPost';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'blog-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {

  posts: IPost[];
  searchString: string = '';
  subs$: Subscription[] = [];
  deleted: boolean = false;

  constructor(
    private postsService: PostsService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.init();
  };

  ngOnDestroy() {
    this.subs$.forEach(s => s.unsubscribe());
    this.subs$ = [];
  }

  init(): void {
    this.fetch();
  }

  fetch(): void {
    const sub$: Subscription = this.postsService.fetch().subscribe((posts: IPost[]) => {
      this.posts = posts;
    });

    this.subs$.push(sub$);
  }

  remove(postId: string): void {
    if(this.deleted) return;

    this.deleted = true;

    const sub$: Subscription = this.postsService.remove(postId).subscribe(() => {
      this.posts = this.posts.filter(p => p.id !== postId);
      this.deleted = false;
      this.alertService.success('Пост удален');
    });

    this.subs$.push(sub$);
  }

}
