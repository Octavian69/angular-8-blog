import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from 'src/app/shared/services/posts.services';
import { Subscription } from 'rxjs';
import { IPost } from 'src/app/shared/interfaces/IPost';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { SimpleObject } from 'src/app/shared/types/Shared';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'blog-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss']
})
export class EditPageComponent implements OnInit, OnDestroy {

  postId: string;
  post: IPost;
  edited: boolean = false;

  form: FormGroup;

  subs$: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private postsService: PostsService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.subs$.forEach(s => s.unsubscribe());
    this.subs$ = [];
  }

  init(): void {
    this.postId = this.activeRoute.snapshot.paramMap.get('id');

    this.initForm();
    this.getPostById(this.postId);
  }

  initForm(): void {
    this.form = this.fb.group({
      title: ['', [Validators.required], []],
      text: ['', [Validators.required], []]
    })
  }

  getErrorClass(controlName: 'title' | 'text'): SimpleObject {
    return {
      'invalid': this.isInvalid(controlName)
    }
  }

  isInvalid(controlName: 'title' | 'text'): boolean {
    const control: AbstractControl = this.form.get(controlName);

    return control.invalid && control.touched;
  }

  getPostById(postId: string) {
    const sub$: Subscription = this.postsService.getPostById(postId).subscribe((post: IPost) => {
      this.post = post;
      this.form.patchValue(post);
    })
  }

  onSubmit() {
    const { value: { text, title }, valid } = this.form;

    if(valid && !this.edited) {
      this.edited = true;

      const editPost: IPost = Object.assign(this.post, { text, title });

      const sub$: Subscription = this.postsService.edit(editPost).subscribe((p: IPost) => {
        this.edited = false;
        this.alertService.success('Пост отредактирован');
      },
        e => this.edited = false
      );

      this.subs$.push(sub$);
    }
  }


}
