import { Component, OnInit, OnDestroy } from '@angular/core';
import { SimpleObject } from 'src/app/shared/types/Shared';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IPost } from 'src/app/shared/interfaces/IPost';
import { Subscription } from 'rxjs';
import { PostsService } from 'src/app/shared/services/posts.services';

@Component({
  selector: 'blog-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  submitted: boolean = false;
  subs$: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private postsService: PostsService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.subs$.forEach(s => s.unsubscribe());
  }

  init(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      title: ['', [Validators.required], []],
      text: ['', [Validators.required], []],
      author: ['', [Validators.required]]
    })
  }

  getErrorClass(controlName: 'title' | 'author'): SimpleObject {
    return {
      'invalid': this.isInvalid(controlName)
    }
  }

  isInvalid(controlName: 'title' | 'author'): boolean {
    const control: AbstractControl = this.form.get(controlName);

    return control.invalid && control.touched;
  }
  
  onSubmit() {
    const { value: { title, author, text }, valid } = this.form;
    
    if(valid) {
      this.submitted = true;
      const date: Date = new Date();
      const post: IPost = { title, author, text, date };
      
      const sub$: Subscription = this.postsService.create(post).subscribe((create: IPost) => {
        console.log(create);
        this.submitted = false;
        this.form.reset();
      },
        e => this.submitted = false
      )

      this.subs$.push(sub$);
    }
  }

}
