import { Component, OnInit } from '@angular/core';
import { PostsService } from '../shared/services/posts.services';
import { Observable } from 'rxjs';
import { IPost } from '../shared/interfaces/IPost';

@Component({
  selector: 'blog-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  post$: Observable<IPost[]> = this.postsService.fetch();

  constructor(
    private postsService: PostsService
  ) { }

  ngOnInit() {
  }

}
