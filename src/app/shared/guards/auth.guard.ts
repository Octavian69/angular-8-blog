import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    
    constructor(
        private router: Router,
        private authService: AuthService
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean
    {
        const isAuth: boolean = this.authService.isAuthenticated();

        if(!isAuth) {
            this.router.navigate(['/admin'], { queryParams: { accessDenied: true } })
            return false;
        }

        return true
    }
}