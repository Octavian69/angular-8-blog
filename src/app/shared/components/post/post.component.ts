import { Component, OnInit, Input } from '@angular/core';
import { IPost } from '../../interfaces/IPost';

@Component({
  selector: 'blog-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input('post') post: IPost;

  constructor() { }

  ngOnInit() {
  }

}
