import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { SimpleObject, AlertType } from '../../types/Shared';
import { AlertService } from '../../services/alert.service';
import { Subscription } from 'rxjs';
import { IAlert } from '../../interfaces/IAlert';

@Component({
  selector: 'blog-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {
  
  @Input('delay') delay: number = 5000;

  type: AlertType = 'success';
  text: string;

  sub$: Subscription;

  constructor(
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    if(this.sub$) this.sub$.unsubscribe();
  }

  init(): void {
     this.initAlertSub()
  }

  initAlertSub(): void {
    this.sub$ = this.alertService.alert$.subscribe(this.initAlert);
  }
  
  getAlertClass(): SimpleObject {
    return {
      'alert-success': this.type === 'success',
      'alert-warning': this.type === 'warning',
      'alert-danger': this.type === 'danger'
    }
  }

  initAlert = (alert: IAlert): void => {
    const { type, text } = alert;

    this.type = type;
    this.text = text;

    const timeout = setTimeout(() => {
      this.text = null;
      clearTimeout(timeout);
    }, this.delay)
  }

  reset(): void {
    this.text = null;
  }
  

}
