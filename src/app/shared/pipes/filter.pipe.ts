import { Pipe, PipeTransform } from '@angular/core';
import { IPost } from '../interfaces/IPost';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {
    transform(posts: IPost[], str: string = ''): IPost[] {
        return posts.filter(p => p.title.toLowerCase().includes(str.toLowerCase()));
    }
}