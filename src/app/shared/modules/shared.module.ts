import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';

const declarations = [];
const initModules = [
    QuillModule.forRoot()
]
const imports = [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
];
const exports = [
    ...declarations,
    ...imports,
    QuillModule
];

@NgModule({
    declarations,
    imports: [...imports, ...initModules],
    exports
})
export class SharedModule {}