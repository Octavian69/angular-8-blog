import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPost } from '../interfaces/IPost';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IFBCreate } from '../interfaces/IFBCreate';
import { IFBPost } from '../interfaces/IFBPost';

@Injectable({providedIn: 'root'})
export class PostsService {
    constructor(
        private http: HttpClient
    ){}

    fetch(): Observable<IPost[]> {
        return this.http.get<IFBPost>(`${environment.fbDbUrl}/posts.json`).pipe(
            map((response: IFBPost) => {
                const posts: IPost[] =  Object.keys(response).reduce((accum, current) => {
                    const id = current;
                    const post = response[id];

                    post.id = id;

                    accum.push(post);

                    return accum;
                }, [])

                return posts;
            })
        )
    }

    getPostById(postId: string): Observable<IPost> {
        return this.http.get<IPost>(`${environment.fbDbUrl}/posts/${postId}.json`).pipe(
            map((post: IPost) => {
                post.id = postId;

                return post;
            })
        )
    }

    create(post: IPost): Observable<IPost> {
        return this.http.post<IFBCreate>(`${environment.fbDbUrl}/posts.json`, post)
        .pipe(
            map((response: IFBCreate) => {
                const { name: id } = response;
                const create: IPost = { ...post, id };  

                return create;
            })
        )
    }

    edit(post: IPost): Observable<IPost> {
        return this.http.patch<IPost>(`${environment.fbDbUrl}/posts/${post.id}.json`, post);
    }

    remove(postId: string): Observable<IPost> {
        return this.http.delete<IPost>(`${environment.fbDbUrl}/posts/${postId}.json`)
    }
    
}