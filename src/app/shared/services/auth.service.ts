import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { Observable, Subject, throwError } from 'rxjs';
import { IUser } from '../interfaces/iUser';
import { environment } from 'src/environments/environment';
import { IFBAuth } from '../interfaces/IFBAuth';

@Injectable({ providedIn: 'root' })
export class AuthService {

    error$: Subject<string> = new Subject()
    
    constructor(
        private http: HttpClient
    ) {}

    get token() {
        const expiredTime: number = +localStorage.getItem('fb-token-exp');
        const isExpired: boolean = new Date().getTime() > expiredTime;

        if(isExpired) {
            this.logout();

            return null;
        } 

        return localStorage.getItem('fb-token');
    }

    login(user: IUser): Observable<any> {
        return this.http.post<any>(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, user)
        .pipe(
            catchError((error: HttpErrorResponse) => {
                this.handleError(error);
                return throwError(error);
            }),
            tap(this.setToken)
        )
    }

    logout(): void {
        localStorage.clear();
    }

    isAuthenticated(): boolean {
        return this.token ? true : false;
    }

    handleError(error: HttpErrorResponse): void {
        const { message } = error.error.error;

        let messageError: string;

        switch(message) {
            case 'INVALID_PASSWORD': {
                messageError = 'Неверный пароль';
                break;
            }
            case 'INVALID_EMAIL': {
                messageError = 'Неверный email';
                break;
            }
            case 'EMAIL_NOT_FOUND': {
                messageError = 'Такой пользователь не зарегистрирован';
                break;
            }
            default: {
                messageError = 'Ошибка, попробуйте еще раз.'
            }
        }

        this.error$.next(messageError);
    }

    setToken = (response: IFBAuth): void => {
        const { expiresIn, idToken } = response;
        const expiredTime: number = new Date().getTime() + (Number(expiresIn) * 1000);

        localStorage.setItem('fb-token', idToken);
        localStorage.setItem('fb-token-exp', String(expiredTime));
    }
}