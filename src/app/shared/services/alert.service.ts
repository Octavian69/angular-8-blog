import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IAlert } from '../interfaces/IAlert';

@Injectable()
export class AlertService {
    alert$: Subject<IAlert> = new Subject<IAlert>();

    success(text: string): void {
        this.alert$.next({ text, type: 'success' })
    }

    warning(text: string): void {
        this.alert$.next({ text, type: 'warning' })
    }

    danger(text: string): void {
        this.alert$.next({ text, type: 'danger' })
    }
}