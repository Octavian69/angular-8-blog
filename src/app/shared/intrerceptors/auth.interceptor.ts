import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { SimpleObject } from '../types/Shared';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private authService: AuthService,
        private router: Router
    ) {}
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if(this.authService.isAuthenticated()) {
            req = req.clone({
                setParams: {
                    auth: this.authService.token
                }
            })
        }       

        return next.handle(req).pipe(catchError((error: HttpErrorResponse) => {
            
            if(error.status === 401) {
                this.authService.logout();
                this.router.navigateByUrl('/admin/login', { queryParams: { authFailed: true } });
            }

            return throwError(error);
        }))
    }
}