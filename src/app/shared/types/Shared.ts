export type SimpleObject = {
    [key: string]: any
}

export type AlertType = 'success' | 'warning' | 'danger';