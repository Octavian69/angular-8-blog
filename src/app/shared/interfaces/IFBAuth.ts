//interface FireBase Authenticated Response
export interface IFBAuth {
    idToken: string; 
    expiresIn: string; 
}