import { AlertType } from "../types/Shared";

export interface IAlert {
    type: AlertType;
    text: string;
}

abstract class Tech {
    constructor(public type: string){}

    abstract getType(): void;
}

abstract class Car extends Tech {
    constructor(public model: string, public price: string) {
        super("Metal")
    }

    getPrice(): string {
        return this.price;
    }

    getModel(): string {
        return this.model
    }

    getType(): void {
        console.log(this.type);
    }
}

class BMW extends Car {

    constructor(model: string, price: string) {
        super(model, price);
    }
    
    logBwmInf(): void {
        console.log(`
            Model: ${this.getModel()},
            Price: ${this.getPrice()}
        `)
    }
}


const bmw: BMW = new BMW('BMW', '55000');

bmw.getPrice();

