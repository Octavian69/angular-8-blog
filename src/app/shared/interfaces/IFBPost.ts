import { IPost } from './IPost';

export interface IFBPost {
    [key: string]: IPost
}