export interface IPost {
    title: string;
    author: string;
    text: string;
    date: Date;
    id?: string;
}